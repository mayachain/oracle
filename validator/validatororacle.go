package validatororacle

import (
	"context"
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"math"
	"net/http"
	"sync"
	"time"

	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/rs/zerolog"
	"google.golang.org/grpc"

	config "price-feeder/config/validator"
	pfsync "price-feeder/pkg/sync"
	"price-feeder/validator/client"
	"price-feeder/validator/provider"

	oracletypes "github.com/Team-Kujira/core/x/oracle/types"
	_ "gitlab.com/mayachain/aztec/x/validatororacle/types"
	validatororacletypes "gitlab.com/mayachain/aztec/x/validatororacle/types"

	"github.com/cosmos/cosmos-sdk/telemetry"
)

// We define tickerSleep as the minimum timeout between each oracle loop. We
// define this value empirically based on enough time to collect exchange rates,
// and broadcast pre-vote and vote transactions such that they're committed in
// at least one block during each voting period.
const (
	tickerSleep = 1000 * time.Millisecond
)

// PreviousPrevote defines a structure for defining the previous prevote
// submitted on-chain.
type PreviousPrevote struct {
	ValidatorList     string
	Salt              string
	SubmitBlockHeight int64
}

func NewPreviousPrevote() *PreviousPrevote {
	return &PreviousPrevote{
		Salt:              "",
		ValidatorList:     "",
		SubmitBlockHeight: 0,
	}
}

// ValidatorOracle implements the core component responsible for fetching exchange rates
// for a given set of currency pairs and determining the correct exchange rates
// to submit to the on-chain price oracle adhering the oracle specification.
type ValidatorOracle struct {
	logger zerolog.Logger
	closer *pfsync.Closer

	providerTimeout       time.Duration
	valProviders          map[provider.Name]provider.Provider
	previousPrevote       *PreviousPrevote
	previousVotePeriod    float64
	validatorOracleClient client.ValidatorOracleClient
	endpoints             map[provider.Name]provider.Endpoint

	mtx           sync.RWMutex
	lastValSyncTS time.Time
	valdiators    string
	paramCache    ParamCache
	healthchecks  map[string]http.Client
}

func New(
	logger zerolog.Logger,
	oc client.ValidatorOracleClient,
	providerTimeout time.Duration,
	endpoints map[provider.Name]provider.Endpoint,
	healthchecksConfig []config.Healthchecks,
) *ValidatorOracle {

	healthchecks := make(map[string]http.Client)
	for _, healthcheck := range healthchecksConfig {
		timeout, err := time.ParseDuration(healthcheck.Timeout)
		if err != nil {
			logger.Warn().
				Str("timeout", healthcheck.Timeout).
				Msg("failed to parse healthcheck timeout, skipping configuration")
		} else {
			healthchecks[healthcheck.URL] = http.Client{
				Timeout: timeout,
			}
		}
	}

	return &ValidatorOracle{
		logger:          logger.With().Str("module", "oracle").Logger(),
		closer:          pfsync.NewCloser(),
		previousPrevote: nil,
		providerTimeout: providerTimeout,
		valProviders:    make(map[provider.Name]provider.Provider),
		paramCache:      ParamCache{},
		endpoints:       endpoints,
		healthchecks:    healthchecks,
	}
}

// Start starts the oracle process in a blocking fashion.
func (o *ValidatorOracle) Start(ctx context.Context) error {
	for {
		select {
		case <-ctx.Done():
			o.closer.Close()

		default:
			o.logger.Debug().Msg("starting oracle tick")

			startTime := time.Now()

			if err := o.tick(ctx); err != nil {
				telemetry.IncrCounter(1, "failure", "tick")
				o.logger.Err(err).Msg("oracle tick failed")
			}

			o.lastValSyncTS = time.Now()

			telemetry.MeasureSince(startTime, "runtime", "tick")
			telemetry.IncrCounter(1, "new", "tick")

			time.Sleep(tickerSleep)
		}
	}
}

// Stop stops the oracle process and waits for it to gracefully exit.
func (o *ValidatorOracle) Stop() {
	o.closer.Close()
	<-o.closer.Done()
}

// GetLastValSyncTimestamp returns the latest timestamp at which validator where
func (o *ValidatorOracle) GetLastValSyncTimestamp() time.Time {
	o.mtx.RLock()
	defer o.mtx.RUnlock()

	return o.lastValSyncTS
}

func (o *ValidatorOracle) setValidators(ctx context.Context) error {
	provider, err := o.getOrSetProvider(ctx)
	if err != nil {
		return err
	}
	valdiators, err := provider.GetWhitelistValidators()
	if err != nil {
		return err
	}
	o.valdiators = valdiators

	return nil

}

func (o *ValidatorOracle) GetValidators() string {
	return o.valdiators

}

// // GetPrices returns a copy of the current prices fetched from the oracle's
// // set of exchange rate providers.
// func (o *ValidatorOracle) GetPrices() sdk.DecCoins {
// 	o.mtx.RLock()
// 	defer o.mtx.RUnlock()
// 	// Creates a new array for the prices in the oracle
// 	prices := sdk.NewDecCoins()
// 	for k, v := range o.prices {
// 		// Fills in the prices with each value in the oracle
// 		prices = prices.Add(sdk.NewDecCoinFromDec(k, v))
// 	}

// 	return prices
//}

// SetPrices retrieves all the prices and candles from our set of providers as
// determined in the config. If candles are available, uses TVWAP in order
// to determine prices. If candles are not available, uses the most recent prices
// with VWAP. Warns the the user of any missing prices, and filters out any faulty
// providers which do not report prices or candles within 2𝜎 of the others.
// func (o *Oracle) SetPrices(ctx context.Context) error {
// 	g := new(errgroup.Group)
// 	mtx := new(sync.Mutex)
// 	providerPrices := make(provider.AggregatedProviderPrices)
// 	providerCandles := make(provider.AggregatedProviderCandles)
// 	requiredRates := make(map[string]struct{})

// 	for providerName, currencyPairs := range o.providerPairs {
// 		providerName := providerName
// 		currencyPairs := currencyPairs

// 		priceProvider, err := o.getOrSetProvider(ctx, providerName)
// 		if err != nil {
// 			return err
// 		}

// 		for _, pair := range currencyPairs {
// 			if _, ok := requiredRates[pair.Base]; !ok {
// 				requiredRates[pair.Base] = struct{}{}
// 			}
// 		}

// 		g.Go(func() error {
// 			prices := make(map[string]types.TickerPrice, 0)
// 			candles := make(map[string][]types.CandlePrice, 0)
// 			ch := make(chan struct{})
// 			errCh := make(chan error, 1)

// 			go func() {
// 				defer close(ch)
// 				prices, err = priceProvider.GetTickerPrices(currencyPairs...)
// 				if err != nil {
// 					telemetry.IncrCounter(1, "failure", "provider", "type", "ticker")
// 					errCh <- err
// 				}

// 				candles, err = priceProvider.GetCandlePrices(currencyPairs...)
// 				if err != nil {
// 					telemetry.IncrCounter(1, "failure", "provider", "type", "candle")
// 					errCh <- err
// 				}
// 			}()

// 			select {
// 			case <-ch:
// 				break
// 			case err := <-errCh:
// 				return err
// 			case <-time.After(o.providerTimeout):
// 				telemetry.IncrCounter(1, "failure", "provider", "type", "timeout")
// 				return fmt.Errorf("provider timed out: %s", providerName)
// 			}

// 			// flatten and collect prices based on the base currency per provider
// 			//
// 			// e.g.: {ProviderKraken: {"ATOM": <price, volume>, ...}}
// 			mtx.Lock()
// 			for _, pair := range currencyPairs {
// 				success := SetProviderTickerPricesAndCandles(providerName, providerPrices, providerCandles, prices, candles, pair)
// 				if !success {
// 					mtx.Unlock()
// 					return fmt.Errorf("failed to find any exchange rates in provider responses")
// 				}
// 			}

// 			mtx.Unlock()
// 			return nil
// 		})
// 	}

// 	if err := g.Wait(); err != nil {
// 		o.logger.Debug().Err(err).Msg("failed to get ticker prices from provider")
// 	}

// 	computedPrices, err := GetComputedPrices(
// 		o.logger,
// 		providerCandles,
// 		providerPrices,
// 		o.providerPairs,
// 		o.deviations,
// 	)
// 	if err != nil {
// 		return err
// 	}

// 	if len(computedPrices) != len(requiredRates) {
// 		return fmt.Errorf("unable to get prices for all exchange candles")
// 	}
// 	for base := range requiredRates {
// 		if _, ok := computedPrices[base]; !ok {
// 			return fmt.Errorf("reported prices were not equal to required rates, missed: %s", base)
// 		}
// 	}

// 	o.prices = computedPrices
// 	return nil
// }

// GetComputedPrices gets the candle and ticker prices and computes it.
// It returns candles' TVWAP if possible, if not possible (not available
// or due to some staleness) it will use the most recent ticker prices
// and the VWAP formula instead.
// func GetComputedPrices(
// 	logger zerolog.Logger,
// 	providerCandles provider.AggregatedProviderCandles,
// 	providerPrices provider.AggregatedProviderPrices,
// 	providerPairs map[provider.Name][]types.CurrencyPair,
// 	deviations map[string]sdk.Dec,
// ) (prices map[string]sdk.Dec, err error) {
// 	// convert any non-USD denominated candles into USD
// 	convertedCandles, err := convertCandlesToUSD(
// 		logger,
// 		providerCandles,
// 		providerPairs,
// 		deviations,
// 	)
// 	if err != nil {
// 		return nil, err
// 	}

// 	// filter out any erroneous candles
// 	filteredCandles, err := FilterCandleDeviations(
// 		logger,
// 		convertedCandles,
// 		deviations,
// 	)
// 	if err != nil {
// 		return nil, err
// 	}

// 	// attempt to use candles for TVWAP calculations
// 	tvwapPrices, err := ComputeTVWAP(filteredCandles)
// 	if err != nil {
// 		return nil, err
// 	}

// 	// If TVWAP candles are not available or were filtered out due to staleness,
// 	// use most recent prices & VWAP instead.
// 	if len(tvwapPrices) == 0 {
// 		convertedTickers, err := convertTickersToUSD(
// 			logger,
// 			providerPrices,
// 			providerPairs,
// 			deviations,
// 		)
// 		if err != nil {
// 			return nil, err
// 		}

// 		filteredProviderPrices, err := FilterTickerDeviations(
// 			logger,
// 			convertedTickers,
// 			deviations,
// 		)
// 		if err != nil {
// 			return nil, err
// 		}

// 		vwapPrices, err := ComputeVWAP(filteredProviderPrices)
// 		if err != nil {
// 			return nil, err
// 		}

// 		return vwapPrices, nil
// 	}

// 	return tvwapPrices, nil
// }

// GetParamCache returns the last updated parameters of the x/oracle module
// if the current ParamCache is outdated, we will query it again.
func (o *ValidatorOracle) GetParamCache(ctx context.Context, currentBlockHeigh int64) (validatororacletypes.Params, error) {
	if !o.paramCache.IsOutdated(currentBlockHeigh) {
		return *o.paramCache.params, nil
	}

	params, err := o.GetParams(ctx)
	if err != nil {
		return validatororacletypes.Params{}, err
	}

	o.paramCache.Update(currentBlockHeigh, params)
	return params, nil
}

// GetParams returns the current on-chain parameters of the x/oracle module.
func (o *ValidatorOracle) GetParams(ctx context.Context) (validatororacletypes.Params, error) {
	grpcConn, err := grpc.Dial(
		o.validatorOracleClient.GRPCEndpoint,
		// the Cosmos SDK doesn't support any transport security mechanism
		grpc.WithInsecure(),
		grpc.WithContextDialer(dialerFunc),
	)
	if err != nil {
		return validatororacletypes.Params{}, fmt.Errorf("failed to dial Cosmos gRPC service: %w", err)
	}

	defer grpcConn.Close()
	queryClient := validatororacletypes.NewQueryClient(grpcConn)

	ctx, cancel := context.WithTimeout(ctx, 15*time.Second)
	defer cancel()

	queryResponse, err := queryClient.Params(ctx, &validatororacletypes.QueryParamsRequest{})
	if err != nil {
		return validatororacletypes.Params{}, fmt.Errorf("failed to get x/oracle params: %w", err)
	}

	return queryResponse.Params, nil
}

func (o *ValidatorOracle) getOrSetProvider(ctx context.Context) (provider.Provider, error) {
	for _, endpoint := range o.endpoints {
		if endpoint.Name == provider.ProviderMaya {
			return provider.NewMayaProvider(endpoint), nil
		}
	}

	return nil, nil
}

func (o *ValidatorOracle) tick(ctx context.Context) error {
	o.logger.Debug().Msg("executing oracle tick")

	blockHeight, err := o.validatorOracleClient.ChainHeight.GetChainHeight()
	if err != nil {
		return err
	}
	if blockHeight < 1 {
		return fmt.Errorf("expected positive block height")
	}

	oracleParams, err := o.GetParamCache(ctx, blockHeight)
	if err != nil {
		return err
	}

	if err := o.setValidators(ctx); err != nil {
		return err
	}

	// Get oracle vote period, next block height, current vote period, and index
	// in the vote period.
	oracleVotePeriod := int64(oracleParams.VotePeriod)
	nextBlockHeight := blockHeight + 1
	currentVotePeriod := math.Floor(float64(nextBlockHeight) / float64(oracleVotePeriod))
	indexInVotePeriod := nextBlockHeight % oracleVotePeriod

	// Skip until new voting period. Specifically, skip when:
	// index [0, oracleVotePeriod - 1] > oracleVotePeriod - 2 OR index is 0
	if (o.previousVotePeriod != 0 && currentVotePeriod == o.previousVotePeriod) ||
		oracleVotePeriod-indexInVotePeriod < 2 {
		o.logger.Info().
			Int64("vote_period", oracleVotePeriod).
			Float64("previous_vote_period", o.previousVotePeriod).
			Float64("current_vote_period", currentVotePeriod).
			Msg("skipping until next voting period")

		return nil
	}

	// If we're past the voting period we needed to hit, reset and submit another
	// prevote.
	if o.previousVotePeriod != 0 && currentVotePeriod-o.previousVotePeriod != 1 {
		o.logger.Info().
			Int64("vote_period", oracleVotePeriod).
			Float64("previous_vote_period", o.previousVotePeriod).
			Float64("current_vote_period", currentVotePeriod).
			Msg("missing vote during voting period")
		telemetry.IncrCounter(1, "vote", "failure", "missed")

		o.previousVotePeriod = 0
		o.previousPrevote = nil
		return nil
	}

	salt, err := GenerateSalt(32)
	if err != nil {
		return err
	}

	valAddr, err := sdk.ValAddressFromBech32(o.validatorOracleClient.ValidatorAddrString)
	if err != nil {
		return err
	}

	exchangeRatesStr := o.GetValidators()
	hash := oracletypes.GetAggregateVoteHash(salt, exchangeRatesStr, valAddr)
	preVoteMsg := &validatororacletypes.MsgAggregateValidatorPrevote{
		Hash:      hash.String(), // hash of prices from the oracle
		Feeder:    o.validatorOracleClient.ValidatorOracleAddrString,
		Validator: valAddr.String(),
	}

	isPrevoteOnlyTx := o.previousPrevote == nil
	if isPrevoteOnlyTx {
		// This timeout could be as small as oracleVotePeriod-indexInVotePeriod,
		// but we give it some extra time just in case.
		//
		// Ref : https://github.com/terra-money/oracle-feeder/blob/baef2a4a02f57a2ffeaa207932b2e03d7fb0fb25/feeder/src/vote.ts#L222
		o.logger.Info().
			Str("hash", hash.String()).
			Str("validator", preVoteMsg.Validator).
			Str("feeder", preVoteMsg.Feeder).
			Msg("broadcasting pre-vote")
		if err := o.validatorOracleClient.BroadcastTx(nextBlockHeight, oracleVotePeriod*2, preVoteMsg); err != nil {
			return err
		}

		currentHeight, err := o.validatorOracleClient.ChainHeight.GetChainHeight()
		if err != nil {
			return err
		}

		o.previousVotePeriod = math.Floor(float64(currentHeight) / float64(oracleVotePeriod))
		o.previousPrevote = &PreviousPrevote{
			Salt:              salt,
			ValidatorList:     exchangeRatesStr,
			SubmitBlockHeight: currentHeight,
		}
	} else {
		// otherwise, we're in the next voting period and thus we vote
		voteMsg := &validatororacletypes.MsgAggregateValidatorVote{
			Salt:              o.previousPrevote.Salt,
			ValidatorsAddress: o.previousPrevote.ValidatorList,
			Feeder:            o.validatorOracleClient.ValidatorOracleAddrString,
			Validator:         valAddr.String(),
		}

		o.logger.Info().
			Str("ValidatorsAddress", voteMsg.ValidatorsAddress).
			Str("validator", voteMsg.Validator).
			Str("feeder", voteMsg.Feeder).
			Msg("broadcasting vote")
		if err := o.validatorOracleClient.BroadcastTx(
			nextBlockHeight,
			oracleVotePeriod-indexInVotePeriod,
			voteMsg,
		); err != nil {
			return err
		}

		o.previousPrevote = nil
		o.previousVotePeriod = 0
		o.healthchecksPing()
	}

	return nil
}

func (o *ValidatorOracle) healthchecksPing() {
	for url, client := range o.healthchecks {
		o.logger.Info().Msg("updating healthcheck status")
		_, err := client.Get(url)
		if err != nil {
			o.logger.Warn().Msg("healthcheck ping failed")
		}
	}
}

// GenerateSalt generates a random salt, size length/2,  as a HEX encoded string.
func GenerateSalt(length int) (string, error) {
	if length == 0 {
		return "", fmt.Errorf("failed to generate salt: zero length")
	}

	bytes := make([]byte, length)

	if _, err := rand.Read(bytes); err != nil {
		return "", err
	}

	return hex.EncodeToString(bytes), nil
}
