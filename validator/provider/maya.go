package provider

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

const (
	mayaRestURL  = "https://stagenet.mayanode.mayachain.info"
	mayaEndpoint = "/thorchain/nodes"
)

type (
	// OsmosisProvider defines an Oracle provider implemented by the Osmosis public
	// API.
	//
	// REF: https://api-osmosis.imperator.co/swagger/
	MayaProvider struct {
		baseURL string
		client  *http.Client
	}

	MayaResponse struct {
		Node string
	}
	MayaNodes struct {
		Data []MayaResponse `json:"data"`
	}

	Keyvalue map[string]interface{}
)

func NewMayaProvider(endpoint Endpoint) *MayaProvider {
	if endpoint.Name == ProviderMaya {
		return &MayaProvider{
			baseURL: endpoint.Rest,
			client:  newDefaultHTTPClient(),
		}
	}
	return &MayaProvider{
		baseURL: mayaRestURL,
		client:  newDefaultHTTPClient(),
	}
}

func (p MayaProvider) GetWhitelistValidators() (string, error) {
	path := fmt.Sprintf("%s%s", p.baseURL, mayaEndpoint)

	resp, err := p.client.Get(path)
	if err != nil {
		return "", fmt.Errorf("failed to make Maya request: %w", err)
	}

	err = checkHTTPStatus(resp)
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()

	bz, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("failed to read Maya response body: %w", err)
	}

	//mayaResp := []any{}
	mayaResp := []Keyvalue{}
	if err := json.Unmarshal(bz, &mayaResp); err != nil {
		return "", fmt.Errorf("failed to unmarshal Osmosis response body: %w", err)
	}

	ret := ""
	for _, node := range mayaResp {
		ret += fmt.Sprintf("%v,", node["bond_address"])
	}

	return ret[:len(ret)-1], nil
}
