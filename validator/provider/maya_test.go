package provider

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMaya(t *testing.T) {
	p := NewMayaProvider(Endpoint{})

	a, _ := p.GetWhitelistValidators()

	path := fmt.Sprintf("%s%s", mayaRestURL, mayaEndpoint)
	fmt.Println(path)
	assert.Equal(t, "", a, path)

}
