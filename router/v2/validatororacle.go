package v2

// Oracle defines the Oracle interface contract that the v1 router depends on.
type ValidatorOracle interface {
	GetValidators() string
}
